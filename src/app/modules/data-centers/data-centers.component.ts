/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Component, OnInit, Inject } from '@angular/core';
import { dataHandling } from '../../shared/services/dataHandling.service';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { snackBarService } from '../../shared/components/snackbar/snackbar.service';
@Component({
  selector: 'app-data-centers',
  templateUrl: './data-centers.component.html',
  styleUrls: ['./data-centers.component.css']
})
export class DataCentersComponent implements OnInit {
  response: any;
  rowData: Observable<any[]>;
  private gridApi;
  private gridColumnApi;
  rowCount: any;
  responsebulkdatacenter: any;
  public overlayLoadingTemplate;
  columnDefs: any[];
  clusterResp: any;
  gridOptions = {
  }
  constructor(public dataHandling: dataHandling, private snackbarService: snackBarService, private router: Router, public dialog: MatDialog) {
    this.setGridColumns();
    this.overlayLoadingTemplate = '<div class="loader"></div>';
  }
  ngOnInit() {
    this.viewDB();
  }
  setGridColumns() {
    this.columnDefs = [
      {
        headerName: 'Data Center Name',
        field: 'datacenter-name',
        suppressNavigable: true,
        cellClass: 'no-border'
      },
      {
        headerName: 'Location',
        field: 'location.city',
        editable: false
      }
    ];
  }
  onGridReady(param) {
    param.api.sizeColumnsToFit();
  }
  countDisplayedRows(params) {
    this.rowCount = params.api.getDisplayedRowCount();
    console.log('this.rowCount', this.rowCount);
  }
  openDialogData() {
    const dialogRef = this.dialog.open(DataDialogElements, {
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
    });
  }
  viewDB() {
    this.dataHandling.viewDataCenter().subscribe(
      (response: any) => {
        this.response = response;
        this.rowData = this.response;
        console.log(this.response)
      }
    );
  }
}
@Component({
  selector: 'data-dialog-elements',
  templateUrl: 'dialoge-bulk-data.html',
  styleUrls: ['./data-centers.component.css']
})
export class DataDialogElements {
  fileContent: string = '';
  dataCenterData: any;
  responsebulkdatacenter: any;
  fileName: string = '';
  extensionChk: boolean;
  noFileUploaded = true;
  constructor(@Inject(MAT_DIALOG_DATA) public data: string, private dialogRef: MatDialogRef<DataDialogElements>, private route: ActivatedRoute, private router: Router, public dataHandling: dataHandling, private snackbarService: snackBarService) {}
  public onChange(fileList: FileList) {
    console.log("inside")
    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    let fileName = fileList[0].name;
    let extension = fileName.split('.').pop().toLowerCase();
    let self = this;
    if (extension != 'json') {
      this.extensionChk = true;
      this.noFileUploaded = true;
      self.fileContent = '';
      self.fileName = '';
    } else {
      this.extensionChk = false;
      this.noFileUploaded = false;
      fileReader.onload = function (x) {
        console.log("inside function")
        self.fileContent = <string>fileReader.result;
        self.fileName = <string>fileName;
      }
      fileReader.readAsText(file);
      console.log(self.fileContent)
    }
  }
  bulkImportDataCenter(data) {
    this.dataHandling.bulkImportDataCenter(data).subscribe(
      (response: any) => {
        this.responsebulkdatacenter = response;
        console.log('this.responsebulkdatacenter', this.responsebulkdatacenter);
        if (this.responsebulkdatacenter[0]['datacenter-id']) {
          const msg = 'Data Center has been imported successfully.';
          this.snackbarService.openSnackBar(msg, 'success', '', '', '');
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate(['/dataCenter']);
        }
        console.log(this.responsebulkdatacenter)
      }, (error) => {
        const msg = 'Data Center could not imported due to some errors.';
        this.snackbarService.openSnackBar(msg, 'error', '', '', '');
      }
    );
  }
  onCancelClick() {
    this.dialogRef.close();
  }
  onSubmit() {
    this.dataCenterData = this.fileContent;
    console.log(this.dataCenterData);
    this.bulkImportDataCenter(this.dataCenterData)
    this.dialogRef.close();
  }
}
