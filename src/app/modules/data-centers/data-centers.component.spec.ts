/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataCentersComponent } from './data-centers.component';

describe('DataCentersComponent', () => {
  let component: DataCentersComponent;
  let fixture: ComponentFixture<DataCentersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataCentersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataCentersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
