/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Component, OnInit } from '@angular/core';
import { dataHandling } from '../../shared/services/dataHandling.service';
import { UserProfile } from 'src/app/shared/models/userProfile';
import { Subscription } from 'rxjs';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  //response: any;
  //userID: number = null;
  claims : any;
  subscription: Subscription;
  //userProfile : UserProfile = null;
  userProfile : UserProfile = {
      userImage: "../../../assets/userProfileImg.png",
      firstName: "Mohammed",
      middleName: "Sameer",
      lastName: "Shaikh",
      selectOrg: "Rakuten",
      authType: "Local Authentication",
      emailID: "shaikh.sameer@rakuten.com",
      userName: "shaikh.sameer",
      phoneNumber: "0987654321",
      location: "Japan"
  };

  constructor(public dataHandling: dataHandling, private oauthService:OAuthService) { }

  ngOnInit(): void {
    // this.dataHandling.getUserProfileData().subscribe(
    //   (response: any) => {
    //     this.userProfile = response;
    //     console.log(this.userProfile);
    //   }
    // );
  }
  get token(){
    this.claims = this.oauthService.getIdentityClaims();
    return this.claims ? this.claims : null;      
  }
  // ngOnDestroy() {
  //   this.subscription.unsubscribe();
  // }
}
