/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, FormGroupDirective, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ErrorStateMatcher } from '@angular/material/core';
import * as CryptoJS from 'crypto-js';
import { dataHandling } from '../../shared/services/dataHandling.service';
import { snackBarService } from '../../shared/components/snackbar/snackbar.service';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatDialog } from '@angular/material';
import { CreateBackupDialogueComponent } from './create-backup-dialogue/create-backup-dialogue.component';
import { OAuthService } from 'angular-oauth2-oidc';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    return !!(control && control.invalid && (control.dirty || control.touched));
  }
}
@Component({
  selector: 'app-create-database',
  templateUrl: './create-database.component.html',
  styleUrls: ['./create-database.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class CreateDatabaseComponent implements OnInit {
  user:any;
  claims: any;
  hide = true;
  panelOpenState = false;
  panelOpenState1 = false;
  selectedDatabase = 'MySql';
  hideconfirm = true;
  message: boolean;
  errmessage: boolean;
  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  selected = '3';
  mySQLselected = 'My SQL 8.0';
  flavorselected = 'flavor';
  charselected = 'UTF-8';
  showLoader: boolean;
  passValue: any;
  notEqual: boolean;
  response: any;
  sqlVersion = '';
  flavors = [];
  choosenFlavor = '-1';
  versions = [];
  newArray: any;
  newArrayCluster: any;
  dataCenter: any;
  clusterVal: any;
  passenc: any;
  dropdownValues: any;
  enableBackup = true;
  backupDetails: any;
  backupPolicy: any;
  incrementalValue: any;
  constructor(
               private _formBuilder: FormBuilder,
               public dialog: MatDialog,
               public dataHandling: dataHandling,
               private router: Router,
               private route: ActivatedRoute,
               private snackbarService: snackBarService,
               private oauthService:OAuthService
             ) {
                this.panelOpenState = false;
                this.panelOpenState1 = false;
                this.getDataCenter();
               }
  matcher = new MyErrorStateMatcher();
  ngOnInit() {
    this.claims = this.oauthService.getIdentityClaims();
    this.user = this.claims['preferred_username']; 
    this.getDropdownData();
    this.message = false;
    this.firstFormGroup = this._formBuilder.group({
      mySQLVal: ['', Validators.required],
      instanceVal: new FormControl('', Validators.compose([
        Validators.pattern(/^[a-z](?!.*--)[a-zA-Z0-9\-]{6,62}(?<!-)$/),
        Validators.required,
      ])),
      userNameVal: ['root'],
      flavorVal: ['', Validators.required],
      deploymentVal: ['Single-DC-Cluster'],
      availVal: ['99.40'],
      charVal: ['', Validators.required],
      password: new FormControl('', Validators.compose([
        Validators.pattern(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{10,32}$/),
        Validators.required,
      ])),
      confirmPass: ['']
    }, { validator: this.checkIfMatchingPasswords('password', 'confirmPass') });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: [''],
      podsVal: ['', Validators.required],
      dataCenter: ['', Validators.required],
      clusterVal: ['', Validators.required]
    });
    console.log(this.firstFormGroup.get('password').value);
  }
  updatePass() {
    this.passenc = CryptoJS.AES.encrypt(this.firstFormGroup.get('password').value, '').toString();
  }
  getDataCenter() {
    this.showLoader = true;
    this.dataHandling.getDataCenter().subscribe(
      (response: any) => {
        this.showLoader = false;
        this.dataCenter = response;
      }
    );
  }
  getCluster(event) {
    console.log(this.secondFormGroup.controls.dataCenter)
    this.showLoader = true;
    this.newArrayCluster = [];
    this.dataHandling.getCluster().subscribe(
      (response: any) => {
        this.showLoader = false;
        this.clusterVal = response;
        this.newArrayCluster = this.clusterVal.filter(opt => opt['datacenter-id'] == event.value.value);
      }
    );
  }
  getDropdownData() {
    this.showLoader = true;
    this.dataHandling.getFlavor().subscribe(
      (response: any) => {
        this.showLoader = false;
        this.dropdownValues = response;
        this.versions = Object.keys(this.dropdownValues);
      }
    );
  }
  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }
  onInput() {
    if (this.firstFormGroup.controls.password.hasError('notEquivalent') || this.firstFormGroup.controls.confirmPass.hasError('notEquivalent'))
      this.notEqual = true;
    else {
      this.notEqual = false;
    }
  }
  openDialogBackup(): void {
    let dataValue;
    if (this.backupDetails) {
      dataValue = { fullBackup: this.backupDetails.fullBackup, incrementalBackup: this.backupDetails.incrementBackup, retention: this.backupDetails.retention }
    } else {
      dataValue = {}
    }
    let dialogRef = this.dialog.open(CreateBackupDialogueComponent, {
      width: '608px',
      height: '430px',
      data: dataValue
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.action === 'submit') {
        this.enableBackup = false;
        this.backupDetails = result;
        console.log(result)
      } else {
        this.enableBackup = true;
      }
    });
  }
  ontestChange() {
    this.choosenFlavor = '';
    if (this.sqlVersion == '') {
      this.flavors = [];
      return;
    }
    this.flavors = this.dropdownValues[this.sqlVersion].flavor;
  }
  convertText() {
    let conversionEncryptOutput = CryptoJS.AES.encrypt('securePasswordTe', this.firstFormGroup.get('password').value).toString();
    console.log(this.firstFormGroup.controls.password.value)
    console.log(conversionEncryptOutput)
  }
  btnback() {
    this.message = false;
    this.errmessage = false;
  }
  createDB() {
    let paStr = this.firstFormGroup.get('password').value;
    if (this.backupDetails) {
      if (this.backupDetails.incrementBackup != 'null' && this.backupDetails.incrementBackup != '') {
        this.incrementalValue = {
          "schedule": {
            "recurrence-type": this.backupDetails.incrementBackup,
            "recurrence-interval": 1
          },
          "retention-period": 1
        }
      } else {
        this.incrementalValue = {}
      }
      this.backupPolicy = {
        "enabled": true,
        "incremental": this.incrementalValue,
        "full": {
          "schedule": {
            "recurrence-type": this.backupDetails.fullBackup,
            "recurrence-interval": 1
          },
          "retention-period": this.backupDetails.retention
        },
      }
    } else {
      this.backupPolicy = {
        "enabled": false
      }
    }
    let encPaStr = window.btoa(paStr);
    console.log(this.backupDetails)
    let conversionEncryptOutput = CryptoJS.AES.encrypt(this.firstFormGroup.get('password').value, '').toString();
    const param = {
      "name": this.firstFormGroup.controls.instanceVal.value,
      "shared": true,
      "catalog-version": this.firstFormGroup.controls.mySQLVal.value,
      "catalog-flavour": this.firstFormGroup.controls.flavorVal.value,
      "root-user": this.firstFormGroup.controls.userNameVal.value,
      "root-password": conversionEncryptOutput,
      "character-set": this.firstFormGroup.controls.charVal.value,
      "deployment-type": this.firstFormGroup.controls.deploymentVal.value,
      "replication-factor": parseInt(this.secondFormGroup.controls.podsVal.value),
      "target-clusters": [
        {
          "datacenter": this.secondFormGroup.controls.dataCenter.value.name,
          "k8s-cluster": this.secondFormGroup.controls.clusterVal.value
        }
      ],
      "backup-policy": this.backupPolicy,
      "status": "Creating",
      "created-by": this.user
    };
    this.showLoader = true;
    this.router.navigate(['/viewDB']);
    const msg = this.firstFormGroup.controls.instanceVal.value + ' creation has been successfully initiated.';
    this.snackbarService.openSnackBar(msg, 'success', '', '', '');
    this.dataHandling.createDB(JSON.stringify(param)).subscribe(
      (response: any) => {
        this.response = response;
        this.router.navigate(['/viewDB']).then((navigated: boolean) => {
          if (navigated) {
            const msg = this.firstFormGroup.controls.instanceVal.value + ' has been successfully initiated.';
            this.snackbarService.openSnackBar(msg, 'success', '', '', '');
          }
        });
        console.log(this.response.status);
        console.log(this.response)
      }
    );
  }
}

