/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBackupDialogueComponent } from './create-backup-dialogue.component';

describe('CreateBackupDialogueComponent', () => {
  let component: CreateBackupDialogueComponent;
  let fixture: ComponentFixture<CreateBackupDialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBackupDialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBackupDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
