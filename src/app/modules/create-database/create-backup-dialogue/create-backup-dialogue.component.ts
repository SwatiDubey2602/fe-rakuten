/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { dataHandling } from '../../../shared/services/dataHandling.service';
import { ActivatedRoute, Router } from '@angular/router';
import { snackBarService } from '../../../shared/components/snackbar/snackbar.service';
@Component({
  selector: 'app-create-backup-dialogue',
  templateUrl: './create-backup-dialogue.component.html',
  styleUrls: ['./create-backup-dialogue.component.css']
})
export class CreateBackupDialogueComponent implements OnInit {
  weekDisable: boolean;
  dayDisable: boolean;
  appId: any;
  fullBackup: any;
  response: any;
  showLoader: boolean;
  retention: any;
  incrementBackup: any;
  constructor(public dialogRef: MatDialogRef<CreateBackupDialogueComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private snackbarService: snackBarService, public dataHandling: dataHandling, private _activatedRoute: ActivatedRoute) { }
  ngOnInit() {
    this.fullBackup = this.data.fullBackup;
    this.radioChange(this.fullBackup)
    this.incrementBackup = this.data.incrementalBackup;
    this.retention = this.data.retention;
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit() {
    console.log(this.data)
    this.dialogRef.close({ action: 'submit', fullBackup: this.fullBackup,incrementBackup: this.incrementBackup, retention: this.retention });
  }
  radioChange(e) {
    this.incrementBackup = '';
    this.retention = '';
    if (e === 'Weekly') {
      this.weekDisable = true;
      this.dayDisable = false;
    } else if (e === 'Daily') {
      this.dayDisable = true;
      this.weekDisable = true;
    } else {
      this.weekDisable = false;
      this.dayDisable = false;
    } if (this.dayDisable || this.weekDisable) {
      this.incrementBackup = 'null'
    }
  }
}
