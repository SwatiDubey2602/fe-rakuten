/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { dataHandling } from '../../shared/services/dataHandling.service';
import { Observable, Subscription, timer } from 'rxjs';
import { MatDialog } from '@angular/material';
import { snackBarService } from '../../shared/components/snackbar/snackbar.service';
import {valueService} from '../../shared/services/valueGetSet.service'
import {DomSanitizer,SafeResourceUrl,} from '@angular/platform-browser';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../../shared/components/confirm-dialog/confirm-dialog.component';
@Component({
  selector: 'app-view-db-details',
  templateUrl: './view-db-details.component.html',
  styleUrls: ['./view-db-details.component.css'],
  
})
export class ViewDbDetailsComponent implements OnInit {
  
  appId: any;
  response: any;
  responseCon: any;
  responseDelete: any;
  result: any;
  showLoader: boolean;
  appDBinstancename: any;
  appClustername: any;
  ready: boolean;
  urlLink:any;
  error: boolean;
  other: boolean;
  grafanaInput:any;
  responseRestore:any;
  showDashboardGrafana: boolean = false;
  subscription: Subscription;
  grafanaUrl: SafeResourceUrl;
  everyFiveSeconds: Observable<number> = timer(0, 5000);
  private timer: Observable<any>;
  constructor(private http: HttpClient,public sanitizer:DomSanitizer,private _activatedRoute: ActivatedRoute,public dataSet:valueService, private router: Router, private snackbarService: snackBarService, public dataHandling: dataHandling, public dialog: MatDialog) { }
  ngOnInit() {
   /* this.response = {"id":"0dec62de-a767-4ea0-98b2-4bc853578233","additional-parameters": [
    {
      "GRAFANA_URL": 'hhtps:grafana.com',
    }
  ],"name":"mysql-0345","shared":true,"endpoint":"34.67.82.243","port":3306,"status":"ready","catalog-version":"5.7.33-31.49","catalog-flavour":"small(memory=4G,cpu=400m,disk=1Gi)","root-user":"rootuser","root-password":"U2FsdGVkX1/HnxffPMiNlJj67f2k6C1ON2AMLP79srs=","character-set":"utf8mb4","deployment-type":"Single DC Cluster","replication-factor":3,"created-by":"admin@sys.com","created-time":"2021-08-04T10:15:59.000+00:00","last-modified-time":"2021-08-04T11:02:20.000+00:00","last-heartbeat-time":"2021-08-04T11:02:20.000+00:00","next-heartbeat-time":"2021-08-04T11:02:20.000+00:00","target-clusters":[{"id":"8320512f-40db-4987-b88e-28dab558cc04","datacenter":"dc2","mySqlClusterRequestid":"0dec62de-a767-4ea0-98b2-4bc853578233","k8s-cluster":"k8sdc2"}]} */
    this.appId = this._activatedRoute.snapshot.paramMap.get('id');
 // this.setColor(this.response.status);
 //this.getDataDetails(this.appId);
 
  this.subscription = this.everyFiveSeconds.subscribe(() => {
      this.getDataDetails(this.appId);
      this.viewRestore(this.appId,123);
      console.log(this.subscription.closed)   
    });  
  }
  getDataDetails(id) {
    
  /*   if (this.dataSet.test === 'openDialogue'){
      this.subscription.unsubscribe();
    }  */
    this.dataHandling.getDetailData(id).subscribe(
      (response: any) => {
      
        this.response = response;
        this.setColor(this.response.status);
        this.urlLink = this.response["additional-parameters"].length == 0 ? '' : this.response["additional-parameters"][0]['parameter-value'];
        if(this.urlLink!=''){
          this.showDashboardGrafana = true
          this.grafanaInput = this.urlLink

        } else{
          this.showDashboardGrafana = false
        }
        if (this.response.status.toLowerCase() === 'ready') {
          this.subscription.unsubscribe();
        }
        console.log(this.response);
      }
    );
  }
  viewRestore(id, value) {
    this.dataHandling.restoreGet(id, value).subscribe(
      (response: any) => {
        this.responseRestore = response;
       
      }
    );
  }
  setColor(params) {
    if (params === "ready") {
      this.ready = true;
      this.error = false;
      this.other = false;
    } else if (params === "error") {
      this.ready = false;
      this.error = true;
      this.other = false;
    } else {
      this.ready = false;
      this.error = false;
      this.other = true;
    }
  }
  confirmDialog(): void {
    this.subscription.unsubscribe();
    const message = 'Are you sure you want to Delete';
    const subtitle = this.response.name
    const dialogData = new ConfirmDialogModel("Confirm Action", message, subtitle);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: dialogData
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      this.result = dialogResult;
      if (this.result) {
        this.deleteData();    
      } else {
        this.subscription = this.everyFiveSeconds.subscribe(() => {
          this.getDataDetails(this.appId);
        });
      }
    });
  }
  deleteData() {
    this.showLoader = true;
    this.dataHandling.deleteData(this.response.id).subscribe(
      (response: any) => {
        this.showLoader = false;
        this.responseDelete = response
        console.log(this.responseDelete);
        console.log(this.responseDelete.status.toLowerCase());
        console.log(JSON.stringify(this.responseDelete ));
        console.log(JSON.stringify(this.responseDelete.status.toLowerCase()));
        if (this.responseDelete.status.toLowerCase() === 'delete') {
          const msg = this.response.name + ' has been successfully deleted.';
          this.snackbarService.openSnackBar(msg, 'success', '', '', '');
          this.router.navigate(['/viewDB']);
        }
      }
    );
  }
  showDashboard(grafanaInput){
    this.showLoader = true;
    this.showDashboardGrafana = true;
    this.grafanaUrl = this.sanitizer.bypassSecurityTrustResourceUrl(grafanaInput); 
    const param = {
      "name": this.response.name,
      "shared": true,
      "catalog-version": this.response['catalog-version'],
      "catalog-flavour": this.response['catalog-flavour'],
      "root-user": this.response['root-user'],
      "root-password": this.response['root-password'],
      "character-set": this.response['character-set'],
      "deployment-type": this.response['deployment-type'],
      "replication-factor": this.response['replication-factor'],
      "target-clusters": [
        {
          "datacenter": this.response['target-clusters'][0].datacenter,
          "k8s-cluster": this.response['target-clusters'][0]['k8s-cluster']
        }
      ],
     //"target-clusters": this.response['target-clusters'],
      "backup-policy": this.response['backup-policy'],
      "additional-parameters": [
        {
          "parameter-name": "GRAFANA_URL",
          "parameter-value": grafanaInput
        }
      ],
      "status": "Creating"
    };
    this.dataHandling.postGrafana(this.appId,param).subscribe(
      (response: any) => {
        this.showLoader = false;
        this.response = response
      }
    ); 
   /*  let headers = new HttpHeaders({"Authorization": 'Bearer eyJrIjoiUlVmVG5KZjRhd0ZoYjBoc2R1NWVpZUNrNk1QNWlFazIiLCJuIjoiVXNlcl9hdXRoIiwiaWQiOjF9'});
this.http.get("https://swatid2602.grafana.net/goto/xDeHP4V7k?orgId=1",
{headers:headers,responseType:'text'}).subscribe(
  res =>{
    
    document.getElementById("dash").innerHTML=res;
    console.log(res)
  },
  error=>{
    console.error(error);
  }
) */
    /*  this.dataHandling.getIframe(grafanaInput).subscribe(
      (response: any) => {
       
        this.response = response;
        
        
        console.log(this.response);
      }
    );  */
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
