/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Component, OnInit, VERSION } from '@angular/core';
import { FormControl, Validators, MinLengthValidator, FormBuilder, FormGroup } from "@angular/forms";
import { Router } from '@angular/router';
import { LoginService } from 'src/app/shared/services/login-service.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private formSubmitAttempt: boolean;
  error = false;
  loginForm: FormGroup;
  constructor(private fb: FormBuilder, public router: Router, private loginservice: LoginService) {
  }
  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    })
  }
  get fval() { return this.loginForm.controls; }
  getError(el) {
    switch (el) {
      case 'user':
        if (this.loginForm.get('email').hasError('required')) {
          return 'Username required';
        }
        break;
      case 'pass':
        if (this.loginForm.get('password').hasError('required')) {
          return 'Password required';
        }
        break;
      default:
        return '';
    }
  }
  login() {
    if (this.loginForm.invalid) {
      return;
    }
    this.loginservice.authenticate(this.fval.email.value, this.fval.password.value)
      .subscribe(
        matched => {
          if (matched == "OK") {
            localStorage.setItem('currentUser', JSON.stringify(this.fval.email.value));
            this.router.navigate(['/viewDB']);
            this.formSubmitAttempt = true;
          } else {
            this.error = true;
          }
        }
      );
  }
}
