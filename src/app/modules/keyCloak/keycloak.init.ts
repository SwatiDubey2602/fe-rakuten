/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

// import { KeycloakService } from "keycloak-angular";
// export function initializeKeycloak(keycloak: KeycloakService): () => Promise<boolean> {

//   return () =>
//     keycloak.init({
//       config: {
//         url: 'https://stg-s-oss.rmb-lab.jp/auth/',
//         realm: 'RCP-SB',
//         clientId: 'PaaS',
//       },
//       initOptions: {
//         responseMode: 'fragment',
//         onLoad: 'login-required',
//         checkLoginIframe: false,
//       },
//       loadUserProfileAtStartUp: true,
//       enableBearerInterceptor: true,
//     });
// }

