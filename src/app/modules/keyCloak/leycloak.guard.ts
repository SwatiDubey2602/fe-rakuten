/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

// import { Injectable } from '@angular/core';
// import { OAuthService, NullValidationHandler, AuthConfig } from 'angular-oauth2-oidc';
// import { JwksValidationHandler } from 'angular-oauth2-oidc';
// import {
//   ActivatedRouteSnapshot,
//   Router,
//   RouterStateSnapshot,
// } from '@angular/router';
// import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';

// @Injectable({
//   providedIn: 'root',
// })
// export class AuthGuard extends KeycloakAuthGuard {
//   private keycloakAuth: Keycloak.KeycloakInstance;
//   constructor(
//     protected readonly router: Router,
//     protected readonly keycloak: KeycloakService
//   ) {
//     super(router, keycloak);
   
//   }
//  /*  authConfig: AuthConfig = {
//     issuer: 'http://localhost:8080/auth/realms/Rakuten',
//     redirectUri: window.location.origin + "/Rakuten",
//     clientId: 'rakuten',
//     scope: 'openid profile email offline_access heroes',
//     responseType: 'code',
//     // at_hash is not present in JWT token
//     disableAtHashCheck: true,
//     showDebugInformation: true
//   } */
 
//   public async isAccessAllowed(
//     route: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot
//   ) {
   
//     // Force the user to log in if currently unauthenticated.
//     if (!this.authenticated) {
//       await this.keycloak.login({
//         redirectUri: window.location.origin + state.url,
//       });
//     }

//     // Get the roles required from the route.
//     const requiredRoles = route.data.roles;

//     // Allow the user to to proceed if no additional roles are required to access the route.
//     if (!(requiredRoles instanceof Array) || requiredRoles.length === 0) {
//       return true;
//     }

//     // Allow the user to proceed if all the required roles are present.
//     return requiredRoles.every((role) => this.roles.includes(role));
//   }
 
// }
