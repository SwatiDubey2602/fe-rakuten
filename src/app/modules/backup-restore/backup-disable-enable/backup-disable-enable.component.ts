/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { dataHandling } from '../../../shared/services/dataHandling.service';
import { snackBarService } from '../../../shared/components/snackbar/snackbar.service';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-backup-disable-enable',
  templateUrl: './backup-disable-enable.component.html',
  styleUrls: ['./backup-disable-enable.component.css']
})
export class BackupDisableEnableComponent implements OnInit {
  user:any;
  claims: any;
  title: string;
  message: string;
  subTitle: string;
  res: any;
  incrementalCheck: any;
  response: any;
  showLoader: boolean;
  constructor(
              public dialogRef: MatDialogRef<BackupDisableEnableComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogDisableModel,
              private snackbarService: snackBarService,
              public dataHandling: dataHandling,
              private oauthService:OAuthService
            ) {
                this.title = data.title;
                this.message = data.message;
                this.subTitle = data.subtitle;
                dialogRef.disableClose = true;
              }

  ngOnInit() {
    this.claims = this.oauthService.getIdentityClaims();
    this.user = this.claims['preferred_username']; 
  }
  onConfirm() {
    this.showLoader = true;
    if (this.data.res['backup-policy'].incremental.schedule === null) {
      this.incrementalCheck = {}
    } else {
      this.incrementalCheck = {
        "schedule": {
          "recurrence-type": this.data.res['backup-policy'].incremental.schedule['recurrence-type'],
          "recurrence-interval": 1
        },
        "retention-period": this.data.res['backup-policy'].incremental['retention-period']
      }
    }
    const param = {
      "name": this.data.res.name,
      "shared": true,
      "catalog-version": this.data.res['catalog-version'],
      "catalog-flavour": this.data.res['catalog-flavour'],
      "root-user": this.data.res['root-user'],
      "root-password": this.data.res['root-password'],
      "character-set": this.data.res['character-set'],
      "deployment-type": this.data.res['deployment-type'],
      "replication-factor": this.data.res['replication-factor'],
      "target-clusters": [
        {
          "datacenter": this.data.res['target-clusters'][0].datacenter,
          "k8s-cluster": this.data.res['target-clusters'][0]['k8s-cluster']
        }
      ],
      "backup-policy": {
        "enabled": false,
        "incremental": this.incrementalCheck,
        "full": {
          "schedule": {
            "recurrence-type": this.data.res['backup-policy'].full.schedule['recurrence-type'],
            "recurrence-interval": 1
          },
          "retention-period": this.data.res['backup-policy'].full['retention-period']
        }
      },
      "status": this.data.res.status,
      "created-by": this.user
    }
    this.dataHandling.updateBackup(this.data.res.id, param).subscribe(
      (response: any) => {
        this.showLoader = false;
        this.response = response;
        console.log('response', this.response);
        if (this.response['backup-policy'].enabled === false) {
          this.dialogRef.close({ action: 'disabled', response: this.response });
          const msg = 'Backup has been successfully disabled.';
          this.snackbarService.openSnackBar(msg, 'success', '', '', '');
        } else {
          this.dialogRef.close({ action: 'error', response: this.response });
          const msg = 'There is some issue. Kindly try again later.';
          this.snackbarService.openSnackBar(msg, 'error', '', '', '');
        }
      }
    );
  }
  onDismiss(): void {
    this.dialogRef.close(false);
  }
}
export class ConfirmDialogDisableModel {
  constructor(public title: string, public message: string, public subtitle: string, public res: any) {
  }
}
