/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackupDisableEnableComponent } from './backup-disable-enable.component';

describe('BackupDisableEnableComponent', () => {
  let component: BackupDisableEnableComponent;
  let fixture: ComponentFixture<BackupDisableEnableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackupDisableEnableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackupDisableEnableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
