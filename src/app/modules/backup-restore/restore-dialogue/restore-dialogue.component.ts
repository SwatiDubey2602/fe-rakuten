/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { valueService } from '../../../shared/services/valueGetSet.service';
import { dataHandling } from '../../../shared/services/dataHandling.service';
import { ActivatedRoute, Router } from '@angular/router';
import { snackBarService } from '../../../shared/components/snackbar/snackbar.service';
import { RestoreComponent } from '../../../modules/backup-restore/restore/restore.component'
@Component({
  selector: 'app-restore-dialogue',
  templateUrl: './restore-dialogue.component.html',
  styleUrls: ['./restore-dialogue.component.css'],
  providers: [RestoreComponent]
})
export class RestoreDialogueComponent implements OnInit {
  restoreName: any;
  targetName: any;
  restoreValue: any;
  showLoader: boolean;
  restoreId: any;
  targetCluster: any;
  response: any;
  appId: any;
  targetUpdated: any;
  constructor(public dialogRef: MatDialogRef<RestoreDialogueComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, public dataSet: valueService, public restoreComp: RestoreComponent, private router: Router, private snackbarService: snackBarService, private _activatedRoute: ActivatedRoute, public dataHandling: dataHandling) {
    this.appId = this._activatedRoute.snapshot.paramMap.get('id');
  }
  ngOnInit() {
    this.appId = this._activatedRoute.snapshot.paramMap.get('id');
    this.restoreName = this.dataSet._dbName;
    this.restoreId = this.dataSet._ID;
    this.targetName = this.restoreName;
    this.restoreValue = 'Full';
    this.viewDB();
    console.log(this.targetName)
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  viewDB() {
    console.log(this.restoreName)
    this.dataHandling.viewDB().subscribe(
      (response: any) => {
        this.restoreName = this.dataSet._dbName;
        this.restoreId = this.dataSet._ID;
        this.targetName = this.restoreName;
        let newVal = this.targetName;
        this.response = response;
        this.response = this.response.filter(function (obj) {
          return obj.status !== 'Delete';
        });
        this.targetCluster = this.response.filter(function (obj) {
          return obj.name !== newVal;
        });
        console.log(this.response)
      }
    );
  }
  onSubmit() {
    console.log(this.appId)
    this.showLoader = true;
    const param = {
      "backup-id": this.data.backupId,
      "type": null,
      "status": null,
      "restore-target-mysql-cluster-id": this.targetName,
      "restore-start-time": null,
      "restore-end-time": null
    }
    this.dataHandling.restorePost(this.restoreId, this.data.backupId, param).subscribe(
      (response: any) => {
        this.showLoader = false;
        this.response = response;
        this.restoreComp.ngOnInit();
        this.dialogRef.close({ action: 'submit', response: this.response });
        const msg = 'Restore has been successfully configured.';
        this.snackbarService.openSnackBar(msg, 'success', '', '', '');
      }
    );
  }
}
