/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { Component, OnInit } from '@angular/core';
import { dataHandling } from '../../../shared/services/dataHandling.service';
import { snackBarService } from '../../../shared/components/snackbar/snackbar.service';
import { Observable, Subscription, timer } from 'rxjs';
import { MatDialog } from '@angular/material';
import { BackupDialogueComponent } from '../backup-dialogue/backup-dialogue.component';
import { restoreRenderer } from './restore-renderer';
import { ActivatedRoute, Router } from '@angular/router';
import { valueService } from '../../../shared/services/valueGetSet.service';
import { BackupDisableEnableComponent, ConfirmDialogDisableModel } from '../backup-disable-enable/backup-disable-enable.component'
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-backup',
  templateUrl: './backup.component.html',
  styleUrls: ['./backup.component.css']
})
export class BackupComponent implements OnInit {
  user:any;
  claims: any;
  columnDefsBackup: any[];
  columnDefsRestore: any[];
  showLoader: boolean;
  response: any;
  showContainer: boolean = false;
  backupPolicyResponse: any;
  rowDataBackup: any;
  rowDataRestore: any;
  dbName: any;
  enableBackup: boolean;
  public frameworkComponents;
  rowData: Observable<any[]>;
  appId: any;
  dbNameValue: any;
  subscription: Subscription;
  incrementalCheck: any;
  everyFiveSeconds: Observable<number> = timer(0, 5000);
  gridOptions = {
    onCellMouseOver: true,
    rowSelection: 'single',
    suppressRowClickSelection: true
  }
  constructor(
              public dataHandling: dataHandling,
              public dataSet: valueService,
              private _activatedRoute: ActivatedRoute,
              private snackbarService: snackBarService,
              public dialog: MatDialog,
              private oauthService:OAuthService
            ) {
                this.setGridColumns();
                this.frameworkComponents = {
                  restoreRenderer: restoreRenderer,
                };
              }
  ngOnInit() {
    this.claims = this.oauthService.getIdentityClaims();
    this.user = this.claims['preferred_username']; 
    this.appId = this._activatedRoute.snapshot.paramMap.get('id');
    this.getDataDetails(this.appId);
    this.subscription = this.everyFiveSeconds.subscribe(() => {
      this.viewBackup(this.appId);
    });
  }
  onGridReady(params) {
    this.dbNameValue = this.dbName.name;
  }
  setGridColumns() {
    this.columnDefsBackup = [
      {
        headerName: 'Status',
        field: 'status',
        width: 120
      },
      {
        headerName: 'Backup ID',
        field: 'backup-id',
        width: 310
      },
      {
        headerName: 'Base Full Backup Id',
        field: 'base-full-backup-id',
        width: 200,
      },
      {
        headerName: 'Backup Type',
        field: 'type',
        suppressNavigable: true,
        enableButton: false,
        cellClass: 'no-border',
        width: 130
      },
      {
        headerName: 'Backup Path',
        field: 'base-location',
        suppressNavigable: true,
        cellClass: 'no-border',
        width: 310
      },
      {
        headerName: 'Start Time',
        field: 'backup-start-time',
        suppressNavigable: true,
        cellClass: 'no-border',
        width: 230

      },
      {
        headerName: 'End Time',
        field: 'backup-end-time',
        suppressNavigable: true,
        cellClass: 'no-border',
        width: 230
      },
      {
        headerName: '',
        dbName: this.dbNameValue,
        cellRendererSelector: params => {
          const passDetails = {
            component: "restoreRenderer"
          };
          const failDetails = {
            Component: ''
          };
          if (params.data.type.toLowerCase() === 'full') {
            return passDetails;
          }
          return null;
        },
        suppressCellSelection: true,
        width: 50
      }
    ];
  }
  getDataDetails(id) {
    this.dataHandling.getDetailData(id).subscribe(
      (response: any) => {
        this.dbName = response;
        this.dataSet.dbName = this.dbName.name;
        this.dataSet.ID = this.dbName.id;
        console.log(this.dbName)
        if (this.dbName['backup-policy'].enabled === false) {
          this.enableBackup = true;
        } else if (this.dbName['backup-policy'].enabled = true) {
          this.enableBackup = false;
        }
      }
    );
  }
  openDialogBackup(value): void {
    if (this.dbName["backup-policy"].enabled === false && this.dbName["backup-policy"].full != null && value === 'backup') {
      this.enableBackup = false;
      this.onBackupSubmit();
    } if (value === 'edit' || this.dbName["backup-policy"].full === null) {
      this.dataSet._setValue = "openDialogue";
      let dialogRef = this.dialog.open(BackupDialogueComponent, {
        width: '328px',
        height: '352px',
        data: { response: this.dbName }
      });
      dialogRef.afterClosed().subscribe(result => {
        this.dataSet._setValue = "cancel";
        if (result.action === 'submit') {
          this.enableBackup = false;
          this.dbName = result.response;
          this.showContainer = false;
          this.viewBackup(this.appId);
        }
      });
    }
  }
  onBackupSubmit() {
    console.log(this.dbName)
    this.showLoader = true;
    if (this.dbName["backup-policy"].incremental.schedule === null) {
      this.incrementalCheck = {}
    } else {
      this.incrementalCheck = {
        "schedule": {
          "recurrence-type": this.dbName["backup-policy"].incremental.schedule['recurrence-type'],
          "recurrence-interval": 1
        },
        "retention-period": this.dbName["backup-policy"].incremental['retention-period']
      }
    }
    const param = {
      "name": this.dbName.name,
      "shared": true,
      "catalog-version": this.dbName['catalog-version'],
      "catalog-flavour": this.dbName['catalog-flavour'],
      "root-user": this.dbName['root-user'],
      "root-password": this.dbName['root-password'],
      "character-set": this.dbName['character-set'],
      "deployment-type": this.dbName['deployment-type'],
      "replication-factor": this.dbName['replication-factor'],
      "target-clusters": [
        {
          "datacenter": this.dbName['target-clusters'][0].datacenter,
          "k8s-cluster": this.dbName['target-clusters'][0]['k8s-cluster']
        }
      ],
      "backup-policy": {
        "enabled": true,
        "incremental": this.incrementalCheck,
        "full": {
          "schedule": {
            "recurrence-type": this.dbName["backup-policy"].full.schedule['recurrence-type'],
            "recurrence-interval": 1
          },
          "retention-period": this.dbName["backup-policy"].full['retention-period']
        }
      },
      "status": this.dbName.status,
      "created-by": this.user
    }
    this.dataHandling.updateBackup(this.dbName.id, param).subscribe(
      (response: any) => {
        this.showLoader = false;
        this.response = response;
      }
    );
  }
  openDisable(): void {
    const message = 'Are you sure you want to Disable';
    const subtitle = 'Backup';
    const res = this.dbName;
    const dialogData = new ConfirmDialogDisableModel("Confirm Action", message, subtitle, res);
    const dialogRef = this.dialog.open(BackupDisableEnableComponent, {
      width: '418px',
      height: '326px',
      data: dialogData,
      panelClass: 'my-dialog'
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      console.log(dialogResult.action === 'disabled')
      if (dialogResult.action === 'disabled') {
        this.ngOnInit();
      }
    });
  }
  viewBackup(id) {
    this.dataHandling.viewBackup(id).subscribe(
      (response: any) => {
        this.response = response;
        this.rowDataBackup = this.response;
      }
    );
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
