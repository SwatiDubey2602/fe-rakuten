/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { RestoreDialogueComponent } from '../restore-dialogue/restore-dialogue.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'restore-cell-renderer',
  template: `<a (click)="openDialogRestore()" style="cursor:pointer"><img src="../../../assets/restore.png"></a>
  <div *ngIf="showLoader" class="loader"></div>`,
})
export class restoreRenderer implements ICellRendererAngularComp {
  private params: any;
  showLoader: boolean;
  response: any;
  result: any;
  color:any;
  constructor(public dialog: MatDialog) {
  }
  agInit(params: any): void {
    this.params = params;
    console.log(this.params)
  }
  refresh
  openDialogRestore(): void {
    let dialogRef = this.dialog.open(RestoreDialogueComponent, {
      width: '312px',
      height: '252px',
      data: { backupId:this.params.data['backup-id']
    }
    });
  }
}