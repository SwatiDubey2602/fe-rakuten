/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { Component, OnInit } from '@angular/core';
import { dataHandling } from '../../../shared/services/dataHandling.service';
import { snackBarService } from '../../../shared/components/snackbar/snackbar.service';
import { Observable, Subscription, timer } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-restore',
  templateUrl: './restore.component.html',
  styleUrls: ['./restore.component.css']
})
export class RestoreComponent implements OnInit {
  columnDefsBackup: any[];
  columnDefsRestore: any[];
  showLoader: boolean;
  response: any;
  rowDataBackup: any;
  rowDataRestore: any;
  subscription: Subscription;
  everyFiveSeconds: Observable<number> = timer(0, 5000);
  public frameworkComponents;
  appId: any;
  rowData: Observable<any[]>;
  gridOptions = {
    onCellMouseOver: true,
    rowSelection: 'single',
    suppressRowClickSelection: true,
  }
  constructor(public dataHandling: dataHandling, private snackbarService: snackBarService, private _activatedRoute: ActivatedRoute) {
    this.setGridColumns();
  }
  ngOnInit() {
    this.appId = this._activatedRoute.snapshot.paramMap.get('id');
    this.subscription = this.everyFiveSeconds.subscribe(() => {
      this.viewRestore(this.appId, 123);
    });
  }
  setGridColumns() {
    this.columnDefsRestore = [
      {
        headerName: 'Status',
        field: 'status',
        width: 120
      },
      {
        headerName: 'Restore ID',
        field: 'restore-id',
        width: 320
      },
      {
        headerName: 'Backup ID',
        field: 'backup-id',
        suppressNavigable: true,
        cellClass: 'no-border',
        width: 320
      },
      {
        headerName: 'MySql Cluster',
        field: 'mySqlID',
        suppressNavigable: true,
        cellClass: 'no-border',
        width: 320
      },
      {
        headerName: 'Start Time',
        field: 'restore-start-time',
        suppressNavigable: true,
        cellClass: 'no-border',
        width: 230
      },
      {
        headerName: 'End Time',
        field: 'restore-end-time',
        suppressNavigable: true,
        cellClass: 'no-border',
        width: 230
      }
    ];
  }
  viewRestore(id, value) {
    this.dataHandling.restoreGet(id, value).subscribe(
      (response: any) => {
        this.response = response;
        this.rowDataRestore = this.response;
      }
    );
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
