import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { dataHandling } from '../../../shared/services/dataHandling.service';
import { ActivatedRoute, Router } from '@angular/router';
import { snackBarService } from '../../../shared/components/snackbar/snackbar.service';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-backup-dialogue',
  templateUrl: './backup-dialogue.component.html',
  styleUrls: ['./backup-dialogue.component.css']
})
export class BackupDialogueComponent implements OnInit {
  user:any;
  claims: any;
  weekDisable: boolean;
  dayDisable: boolean;
  appId: any;
  fullBackup: any;
  response: any;
  showLoader: boolean;
  retention: any;
  incrementBackup: any;
  incrementalCheck: any;
  fullCheck:any;
  maxInput:any;
  constructor(
                public dialogRef: MatDialogRef<BackupDialogueComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,
                private snackbarService: snackBarService,
                public dataHandling: dataHandling,
                private _activatedRoute: ActivatedRoute,
                private oauthService:OAuthService
              ) { dialogRef.disableClose = true; }

  ngOnInit() {
    this.claims = this.oauthService.getIdentityClaims();
    this.user = this.claims['preferred_username']; 
    this.fullBackup = this.data.response["backup-policy"].full.schedule["recurrence-type"];
    this.radioChange(this.fullBackup)
    if (this.data.response["backup-policy"].incremental.schedule != null) {
      this.incrementBackup = this.data.response["backup-policy"].incremental.schedule["recurrence-type"];
    } else {
      this.incrementBackup = '';
    }
    this.retention = this.data.response["backup-policy"].full['retention-period'];
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  onSubmit() {
    console.log(this.data)
    this.showLoader = true;
    if (this.incrementBackup === "null") {
      this.incrementalCheck = {}
    } else {
      this.incrementalCheck = {
        "schedule": {
          "recurrence-type": this.incrementBackup,
          "recurrence-interval": 1
        },
        "retention-period": this.retention
      }
    }
    this.fullCheck = {
      "schedule": {
        "recurrence-type": this.fullBackup,
        "recurrence-interval": 1
      },
      "retention-period": this.retention
    }
    const param = {
      "name": this.data.response.name,
      "shared": true,
      "catalog-version": this.data.response['catalog-version'],
      "catalog-flavour": this.data.response['catalog-flavour'],
      "root-user": this.data.response['root-user'],
      "root-password": this.data.response['root-password'],
      "character-set": this.data.response['character-set'],
      "deployment-type": this.data.response['deployment-type'],
      "replication-factor": this.data.response['replication-factor'],
      "target-clusters": [
        {
          "datacenter": this.data.response['target-clusters'][0].datacenter,
          "k8s-cluster": this.data.response['target-clusters'][0]['k8s-cluster']
        }
      ],
      "backup-policy": {
        "enabled": true,
        "incremental": this.incrementalCheck,
        "full": this.fullCheck
      },
      "status": this.data.response.status,
      "created-by": this.user
    }
    this.dataHandling.updateBackup(this.data.response.id, param).subscribe(
      (response: any) => {
        this.showLoader = false;
        this.response = response;
        if (this.response['backup-policy']) {
          this.dialogRef.close({ action: 'submit', response: this.response });
          const msg = 'Backup has been successfully configured.';
          this.snackbarService.openSnackBar(msg, 'success', '', '', '');
        } else {
          this.dialogRef.close({ action: 'submitError', response: this.response });
          const msg = 'There is some issue. Kindly try again later.';
          this.snackbarService.openSnackBar(msg, 'error', '', '', '');
        }
      }
    );
  }
  radioChange(e) {
    this.incrementBackup = '';
    this.retention = '';
    if (e === 'Weekly') {
      this.weekDisable = true;
      this.dayDisable = false;
      this.maxInput = '54';
    } else if (e === 'Daily') {
      this.dayDisable = true;
      this.weekDisable = true;
      this.maxInput = '366';
    } else {
      this.weekDisable = false;
      this.dayDisable = false;
      this.maxInput = '12';
    } if (this.dayDisable || this.weekDisable) {
      this.incrementBackup = 'null'
    }
  }
  checkRetention(retention){
    if(retention > this.maxInput){
     this.retention =  this.maxInput
    }
  }
}
