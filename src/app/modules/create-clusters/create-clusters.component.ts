/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Component, OnInit, Inject } from '@angular/core';
import { dataHandling } from '../../shared/services/dataHandling.service';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { snackBarService } from '../../shared/components/snackbar/snackbar.service';
@Component({
  selector: 'app-create-clusters',
  templateUrl: './create-clusters.component.html',
  styleUrls: ['./create-clusters.component.css']
})
export class CreateClustersComponent implements OnInit {
  showLoader: boolean;
  public response: any;
  public responseData: any;
  rowData: Observable<any[]>;
  private gridApi;
  private gridColumnApi;
  responsebulkcluster: any;
  public overlayLoadingTemplate;
  responsebulkdatacenter: any;
  clusterData: any;
  rowCount: any;
  newArrayCluster: any;
  columnDefs: any[];
  gridOptions = {
  }
  constructor(public dataHandling: dataHandling, private snackbarService: snackBarService, private router: Router, public dialog: MatDialog) {
    this.setGridColumns();
    this.overlayLoadingTemplate =
      '<div class="loader"></div>';
  }
  ngOnInit() {
    this.viewCluster();
  }
  setGridColumns() {
    this.columnDefs = [
      {
        headerName: 'Status',
        field: 'status',
        cellRenderer: params => this.setColor(params)
      },
      {
        headerName: 'Name',
        field: 'cluster-name',
        suppressNavigable: true,
        cellClass: 'no-border'
      },
      {
        headerName: 'Data Center Name',
        field: 'datacenter-name',
        editable: false
      }
    ];
  }
  onGridReady(param) {
    param.api.sizeColumnsToFit();
  }

  countDisplayedRows(params) {
    this.rowCount = params.api.getDisplayedRowCount();
    console.log('this.rowCount', this.rowCount);
  }
  setColor(params) {
    console.log(params)
    if (params.value.toLowerCase() === "ready") {
      return '<span class="green_icon"></span>' + params.value
    } else if (params.value === "error") {
      return '<span class="red_icon"></span>' + params.value
    } else {
      return '<span class="orange_icon"></span>' + params.value
    }
  }
  openDialogCluster() {
    const dialogRef = this.dialog.open(DialogElementsExampleDialog, {
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
    });

  }

  viewCluster() {
    this.dataHandling.viewCluster().subscribe(
      (response: any) => {
        this.response = response
        this.dataHandling.viewDataCenter().subscribe(
          (response: any) => {
            this.responseData = response;
            const result = this.response.map(item => {
              const searchedItem = this.responseData.find(fItem => fItem['datacenter-id'] ===
                item['datacenter-id']);
              if (searchedItem && Object.keys(searchedItem).length > 0) {
                item['datacenter-name'] = searchedItem['datacenter-name']
              } else {
                item['datacenter-name'] = ''
              }
              return item;
            });
            console.log(this.response);
            console.log(this.responseData);
            console.log(result);
            this.rowData = result;
          }
        );
      }
    );
  }
}
@Component({
  selector: 'dialog-elements-example-dialog',
  templateUrl: 'dialoge-bulk.html',
  styleUrls: ['./create-clusters.component.css']
})
export class DialogElementsExampleDialog {
  fileContent: string = '';
  fileName: string = '';
  responsebulkcluster: any;
  clusterData: any;
  showLoader: boolean;
  extensionChk: boolean;
  noFileUploaded = true;
  constructor(@Inject(MAT_DIALOG_DATA) public data: string,
    private dialogRef: MatDialogRef<DialogElementsExampleDialog>, private route: ActivatedRoute, private router: Router, public dataHandling: dataHandling, private snackbarService: snackBarService) { }
  public onChange(fileList: FileList) {
    console.log("inside")
    this.showLoader = true;
    let file = fileList[0];
    let fileReader: FileReader = new FileReader();
    let fileName = fileList[0].name;
    console.log(fileList[0]);
    let extension = fileName.split('.').pop().toLowerCase()
    let self = this;
    if (extension != 'json') {
      this.extensionChk = true;
      this.noFileUploaded = true;
      self.fileContent = '';
      self.fileName = '';
    } else {
      this.extensionChk = false;
      this.noFileUploaded = false;
      fileReader.onload = function (x) {
        console.log("inside function")
        self.fileContent = <string>fileReader.result;
        self.fileName = <string>fileName;
      }
      this.showLoader = false;
      fileReader.readAsText(file);
      console.log(self.fileContent)
    }
  }

  bulkImportCluster(data) {
    this.dataHandling.bulkImportCluster(data).subscribe(
      (response: any) => {
        this.responsebulkcluster = response;
        console.log('this.responsebulkcluster', this.responsebulkcluster);
        if (this.responsebulkcluster[0]['cluster-id']) {
          const msg = 'Cluster has been imported successfully.';
          this.snackbarService.openSnackBar(msg, 'success', '', '', '');
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate(['/clusters']);
        }
        console.log(this.responsebulkcluster)
      }, (error) => {
        const msg = 'Cluster could not imported due to some errors.';
        this.snackbarService.openSnackBar(msg, 'error', '', '', '');
      }
    );
  }
  onCancelClick() {
    this.dialogRef.close();
  }
  onSubmit() {
    this.clusterData = this.fileContent;
    console.log(this.clusterData);
    this.bulkImportCluster(this.clusterData)
    this.dialogRef.close();
  }
}
