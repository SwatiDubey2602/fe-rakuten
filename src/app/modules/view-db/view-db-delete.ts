/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Component } from '@angular/core';
import { dataHandling } from '../../shared/services/dataHandling.service';
import { AgRendererComponent } from 'ag-grid-angular';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../../shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material/snack-bar';
import { snackBarService } from '../../shared/components/snackbar/snackbar.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'delete-cell-renderer',
  template: `<a (click)="confirmDialog()" style="cursor:pointer"><img src="../../../assets/Delete Outline.png"></a>
    <div *ngIf="showLoader" class="loader" style=" position: fixed;left: 50%;top: 50%;height:60px;width:60px;margin:0px auto;-webkit-animation: rotation .6s infinite linear;-moz-animation: rotation .6s infinite linear;-o-animation: rotation .6s infinite linear;animation: rotation .6s infinite linear;border-left: 6px solid #f5dfe6;border-right: 6px solid #ef6b98;border-bottom: 6px solid #C53E6C;border-top: 6px solid #C53E6C;border-radius:100%;"></div>`,
})
export class DeleteValueRenderer implements ICellRendererAngularComp {
  private params: any;
  showLoader: boolean;
  response: any;
  result: any;
  color: any;
  constructor(public dataHandling: dataHandling, private router: Router, private route: ActivatedRoute, private snackbarService: snackBarService, public dialog: MatDialog, private _snackBar: MatSnackBar) {
  }
  agInit(params: any): void {
    this.params = params;
    console.log(this.params.api.getCellRendererInstances(this.params))
  }
  refresh
  btnClickedHandler() {
    console.log(this.params.data.id);
  }
  confirmDialog(): void {
    const message = 'Are you sure you want to Delete';
    const subtitle = this.params.data.name
    const dialogData = new ConfirmDialogModel("Confirm Action", message, subtitle);
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: dialogData
    });
    dialogRef.afterClosed().subscribe(dialogResult => {
      this.result = dialogResult;
      if (this.result) {    
        this.deleteData();
      } else {
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate(['/viewDB']);
      }
    });
  }
  deleteData() {
    this.showLoader = true;
    console.log(this.params.data.id)
    this.dataHandling.deleteData(this.params.data.id).subscribe(
      (response: any) => {
        this.showLoader = false;
        this.response = response;
        console.log(this.response);
        console.log(this.response.status.toLowerCase());
        if (this.response.status.toLowerCase() === 'delete') {
          const msg = this.params.data.name + ' has been successfully deleted.';
          this.snackbarService.openSnackBar(msg, 'success', '', '', '');
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate(['/viewDB']);
        }
      }
    );
  }
}