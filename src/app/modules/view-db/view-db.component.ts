/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Component, OnInit } from '@angular/core';
import { dataHandling } from '../../shared/services/dataHandling.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, Subscription, timer } from 'rxjs';
import { DeleteValueRenderer } from './view-db-delete';
import { ConfirmDialogModel, ConfirmDialogComponent } from '../../shared/components/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { forkJoin, of } from 'rxjs';
@Component({
  selector: 'app-view-db',
  templateUrl: './view-db.component.html',
  styleUrls: ['./view-db.component.css']
})
export class ViewDbComponent implements OnInit {
  showLoader: boolean;
  response: any;
  rowData: Observable<any[]>;
  private gridApi;
  private gridColumnApi;
  public tooltipShowDelay;
  public frameworkComponents;
  public overlayLoadingTemplate;
  rowCount: any;
  columnDefs: any[];
  showBtn: boolean;
  rowIsSelected: boolean;
  subscription: Subscription;
  selectedRow: any;
  result: string = '';
  everyFiveSeconds: Observable<number> = timer(0, 2000);
  private timer: Observable<any>;
  gridOptions = {
    onCellMouseOver: true,
    rowSelection: 'single',
    suppressRowClickSelection: true,
    selectionChanged: (e) => {
      let rowCount = this.gridApi.getSelectedRows();
      if (rowCount.length > 0) {
        this.showBtn = true;
      } else {
        this.showBtn = false;
      }
      console.log(this.gridApi.getSelectedRows());
    }
  }

  constructor(public dataHandling: dataHandling, public dialog: MatDialog, private router: Router, private _snackBar: MatSnackBar) {
    this.setGridColumns();
    this.frameworkComponents = {
      totalValueRenderer: DeleteValueRenderer,
    };
    this.overlayLoadingTemplate = '<div class="loader"></div>';
  }
  countDisplayedRows(params) {
    this.rowCount = params.api.getDisplayedRowCount();
    console.log('this.rowCount', this.rowCount);
  }
  ngOnInit() {
    this.subscription = this.everyFiveSeconds.subscribe(() => {
      this.viewDB();
    });
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }
  setGridColumns() {
    this.columnDefs = [
      {
        headerName: 'Status',
        field: 'status',
        headerCheckboxSelection: true,
        cellRenderer: params => this.setColor(params),
        checkboxSelection: true,
        cellClass: 'statusCapital',
        selectionChanged: params => {
          return this.showBtn = true;
        }
      },
      {
        headerName: 'Name',
        field: 'name',
        suppressNavigable: true,
        cellClass: 'no-border',
        onCellClicked: params => this.onCellClicked(params)
      },
      {
        headerName: 'Deployment Type',
        field: 'deployment-type',
        suppressNavigable: true,
        cellClass: 'no-border',
      },
      {
        headerName: 'Version',
        field: 'catalog-version',
        suppressNavigable: true,
        cellClass: 'no-border',

      },
      {
        headerName: 'Created By',
        field: 'created-by',
        suppressNavigable: true,
        cellClass: 'createdBy',
      },
      {
        headerName: 'Create Date',
        field: 'created-time',
        suppressNavigable: true,
        cellClass: 'no-border',
      },
      {
        headerName: '',
        field: 'delete',
        cellRenderer: 'totalValueRenderer',
        cellrendereparam: params => {
          params.editable = true
        },
        suppressCellSelection: true,
      },
    ];
  }
  onrowClicked(event) {
    this.gridColumnApi.setColumnsVisible(['delete'], true);
    console.log("row")
  }
  setColor(params) {
    console.log(params)
    if (params.value === "ready") {
      return '<span class="green_icon"></span>' + '<p class="paramCapital">' + params.value + '</p>'
    } else if (params.value === "error") {
      return '<span class="red_icon"></span>' + '<p class="paramCapital">' + params.value + '</p>'
    } else {
      return '<span class="orange_icon"></span>' + '<p class="paramCapital">' + params.value + '</p>'
    }
  }
  onGridReady(params) {
    this.rowCount = params.api.getDisplayedRowCount();
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    console.log(this.rowCount);
    params.api.sizeColumnsToFit();
  }
  onCellClicked(event) {
    this.router.navigate(['/createDBDetails/', event.data.id]);
  }
  onCellMouseOver(event) {
    this.rowCount = this.gridApi.getSelectedRows();
    if (this.rowCount.length > 0) {
      this.subscription.unsubscribe();
    } else {
      this.subscription = this.everyFiveSeconds.subscribe(() => {
        this.viewDB();
      });
    }
    console.log(this.gridApi.getSelectedRows());
  }
  deleteData() {
    this.selectedRow = this.gridApi.getSelectedRows();
    this.dataHandling.deleteData(this.selectedRow[0].id).subscribe(
      (response: any) => {
        this.response = response
      }
    );
    if (this.selectedRow.length > 0) {
      this.rowIsSelected = true
    } else {
      this.rowIsSelected = false
    }
    console.log(this.selectedRow)
  }
  viewDB() {
    this.dataHandling.viewDB().subscribe(
      (response: any) => {
        this.response = response;
        this.response = this.response.filter(function (obj) {
          return obj.status !== 'Delete';
        });
        this.rowData = this.response;
        console.log(this.response)
      }
    );
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
