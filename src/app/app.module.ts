/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule,APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateDatabaseComponent } from './modules/create-database/create-database.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FlexLayoutModule } from "@angular/flex-layout";
import {
  MatStepperModule,
  MatInputModule, MatSnackBar, matSnackBarAnimations, MatSnackBarModule,
  MatButtonModule, MatSidenav, MatListModule, MatBadgeModule, MatDialogModule, MatTabsModule,
  MatAutocompleteModule, MatSelectModule, MatToolbarModule, MatExpansionModule, MatExpansionPanel,
  MatIconModule, MatCardModule, MatSliderModule, MatRadioModule,MatMenuModule, MatTableModule, MatTooltipModule, MatDividerModule, MatGridListModule, MatSidenavModule, MatGridList,
} from '@angular/material';

import { HeaderComponent } from './core/components/header/header.component';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { ViewDbComponent } from './modules/view-db/view-db.component';
import { ViewDbDetailsComponent } from './modules/view-db-details/view-db-details.component';
import { SearchComponent } from './shared/components/search/search.component';
import { AgGridModule } from 'ag-grid-angular';
import { BreadcrumbComponent } from './core/components/breadcrumb/breadcrumb.component';
import { BreadcrumbNavService } from './core/components/breadcrumb/breadcrumb-nav.service';
import { snackBarService } from '../../src/app/shared/components/snackbar/snackbar.service';
import { CreateClustersComponent } from './modules/create-clusters/create-clusters.component';
import { DataCentersComponent } from './modules/data-centers/data-centers.component';
import { DialogElementsExampleDialog } from './modules/create-clusters/create-clusters.component';
import { DataDialogElements } from './modules/data-centers/data-centers.component';
import { SnackbarComponent } from './shared/components/snackbar/snackbar.component';
import { DeleteValueRenderer } from './modules/view-db/view-db-delete';
import { restoreRenderer } from './modules/backup-restore/backup/restore-renderer'
import { ConfirmDialogComponent } from './shared/components/confirm-dialog/confirm-dialog.component';
import { encodingPassword } from '../app/shared/pipes/encoding-password.pipe';
import { BackupRestoreComponent } from './modules/backup-restore/backup-restore.component';
import { BackupDialogueComponent } from './modules/backup-restore/backup-dialogue/backup-dialogue.component';
import { RestoreDialogueComponent } from './modules/backup-restore/restore-dialogue/restore-dialogue.component';
import { LoginComponent } from './modules/login/login.component';
import { FileDragNDropDirective } from './shared/file-drag-ndrop.directive';
import { BackupComponent } from './modules/backup-restore/backup/backup.component';
import { RestoreComponent } from './modules/backup-restore/restore/restore.component';
import {valueService} from './shared/services/valueGetSet.service';
import { SafePipe } from '../app/shared/pipes/sanitize-iframe-url.pipe';
import { CreateBackupDialogueComponent } from './modules/create-database/create-backup-dialogue/create-backup-dialogue.component';
import { BackupDisableEnableComponent } from './modules/backup-restore/backup-disable-enable/backup-disable-enable.component';
import { UserProfileComponent } from './modules/userProfile/user-profile.component';
import { AuthConfigModule } from './shared/services/auth-config.module';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AuthInterceptorService } from './shared/services/auth-interceptor.service';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CreateDatabaseComponent,
    HeaderComponent,
    LoaderComponent,
    ViewDbComponent,
    ViewDbDetailsComponent,
    encodingPassword,
    SafePipe,
    SearchComponent,
    BreadcrumbComponent,
    CreateClustersComponent,
    DataCentersComponent,
    DataDialogElements,
    DialogElementsExampleDialog,
    DeleteValueRenderer,
    restoreRenderer,
    SnackbarComponent,
    ConfirmDialogComponent,
    BackupRestoreComponent,
    BackupDialogueComponent,
    RestoreDialogueComponent,
    CreateBackupDialogueComponent,
    FileDragNDropDirective,
    BackupComponent,
    RestoreComponent,
    CreateBackupDialogueComponent,
    BackupDisableEnableComponent,
    UserProfileComponent
  ],
  entryComponents: [DialogElementsExampleDialog, SnackbarComponent,CreateBackupDialogueComponent, DataDialogElements, ConfirmDialogComponent,BackupDisableEnableComponent, BackupDialogueComponent,RestoreDialogueComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule, MatSnackBarModule,
    FlexLayoutModule,
    MatBadgeModule,
    AgGridModule.withComponents([DeleteValueRenderer,restoreRenderer]),
    MatStepperModule,
    MatSelectModule, MatDialogModule,
    MatInputModule,
    MatTooltipModule,
    MatButtonModule,
    MatRadioModule,
    MatTabsModule,
    MatAutocompleteModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatCardModule,
    MatSliderModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule,
    MatTableModule,
    MatListModule, 
    MatExpansionModule,
    MatMenuModule,
    MatGridListModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['http://localhost:4200'],
        sendAccessToken: true
      }
    }),
    AuthConfigModule
    
  ],
  providers: [
      BreadcrumbNavService,
      snackBarService,
      valueService,
      { 
        provide: HTTP_INTERCEPTORS,
        useClass: AuthInterceptorService,
        multi: true
      }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
