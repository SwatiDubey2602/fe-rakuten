/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewDbComponent } from './modules/view-db/view-db.component';
import { CreateDatabaseComponent } from './modules/create-database/create-database.component';
import { ViewDbDetailsComponent } from './modules/view-db-details/view-db-details.component';
import { CreateClustersComponent } from './modules/create-clusters/create-clusters.component';
import { DataCentersComponent } from './modules/data-centers/data-centers.component';
import { LoginComponent } from './modules/login/login.component';
import { UserProfileComponent } from './modules/userProfile/user-profile.component';
import { AuthConfigGuard } from './shared/services/auth-config.guard';

const routes: Routes = [
  {
    path : '',
    redirectTo: 'login', pathMatch: 'full',
  },
  {
    path : 'login',component:LoginComponent,
  },
  // {
  //   path : 'viewDB',
  //   component: ViewDbComponent,
  //   data: { breadcrumb: 'viewDB' },  
  // },
  {
    path : 'userProfile',
    component: UserProfileComponent,
    data: { breadcrumb: 'Profile' },  
  },
  {
    path : 'viewDB',
    component: ViewDbComponent,
    data: { breadcrumb: 'viewDB' },  
   canActivate:[AuthConfigGuard]
  },
  {
    path: 'createDB',
    data: { breadcrumb: 'Create Cluster' },
    children: [
      {
        path: '',
        component: CreateDatabaseComponent,
      },
      {
        path: 'createDB',
        component: CreateDatabaseComponent,
        data: { breadcrumb: 'createDB' },
      }
    ]
  },
  {
    path: 'clusters',
    data: { breadcrumb: 'Clusters' },
    children: [
      {
        path: '',
        component: CreateClustersComponent,
      },
      {
        path: 'clusters',
        component: CreateClustersComponent,
        data: { breadcrumb: 'Clusters' },
      }
    ]
  },
  {
    path: 'dataCenter',
    data: { breadcrumb: 'Data Centers' },
    children: [
      {
        path: '',
        component: DataCentersComponent,
      },
      {
        path: 'dataCenter',
        component: DataCentersComponent,
        data: { breadcrumb: 'Data Centers' },
      }
    ]
  },
  {
    path: 'createDBDetails/:id',
    pathMatch: 'full',
    data: { breadcrumb: 'DB Details' },
    children: [
      {
        path: '',
        component: ViewDbDetailsComponent,
        pathMatch: 'full',
      },
      {
        path: 'createDBDetails/:id',
        component: ViewDbDetailsComponent,
        pathMatch: 'full',
        data: { breadcrumb: 'DB Details' },
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
