/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Component, OnInit, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.css']
})
export class SnackbarComponent implements OnInit {
  iconsVal: any;
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: any) {
    console.log(data);
  }
  ngOnInit() {
  }
  get getIcon() {
    switch (this.data.snackType) {
      case 'success':
        return { type: this.data.snackType, icon: 'check' };
      case 'error':
        return { type: this.data.snackType };
      case 'warn':
        return { type: this.data.snackType, icon: 'warning_outline' };
      case 'info':
        return { type: this.data.snackType, icon: 'info' };
    }
  }
  closeSnackbar() {
    this.data.snackBar.dismiss();
  }

}