/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

export class UserProfile {
    //id: number;
    userImage: string;
    firstName: string;
    middleName: string;
    lastName: string;
    selectOrg: string;
    authType: string;
    emailID: string;
    userName: string;
    phoneNumber: string;
    location: string;
    //token: string;
}