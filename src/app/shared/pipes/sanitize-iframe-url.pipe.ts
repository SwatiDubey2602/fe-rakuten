/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Pipe, PipeTransform,SecurityContext  } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';

@Pipe({ name: 'safe' })
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}
  transform(url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(this.sanitizer.sanitize(SecurityContext.URL, url))
   // return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}