/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Pipe, PipeTransform } from '@angular/core';
@Pipe({name: 'encodingPassword'})
export class encodingPassword implements PipeTransform {
  transform(value: string): string {
  let conversionEncryptOutput = value.replace(/./gi, '*');
    return conversionEncryptOutput;
  }
}