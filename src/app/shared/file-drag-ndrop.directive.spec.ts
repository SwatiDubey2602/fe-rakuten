/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { FileDragNDropDirective } from './file-drag-ndrop.directive';

describe('FileDragNDropDirective', () => {
  it('should create an instance', () => {
    const directive = new FileDragNDropDirective();
    expect(directive).toBeTruthy();
  });
});
