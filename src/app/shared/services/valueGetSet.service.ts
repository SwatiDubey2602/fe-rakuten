/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import {Injectable} from '@angular/core';

@Injectable()
export class valueService {
  public _setValue:any;
  public _dbName:any;
  public _ID:any;
  set test(value:any) {
    this._setValue = value
  }
  get test():any {
    return this._setValue;
  }
  set dbName(value:any){
    this._dbName = value;
  }
  get dbName():any {
    return this._dbName;
  }
  set ID(id:any){
    this._ID = id;
  }
  getId():any{
    return this._ID;
  }
}