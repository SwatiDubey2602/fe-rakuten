import { Injectable, Optional } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
import { OAuthService } from 'angular-oauth2-oidc';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthInterceptorService implements HttpInterceptor {

  constructor(private oauthService: OAuthService) {}

  intercept(req: HttpRequest<any>, next:HttpHandler): Observable<HttpEvent<any>>{

    let accessToken = this.oauthService.getAccessToken();

    // const header = 'Bearer ' + accessToken;
    // const headers = req.headers.set('Authorization', header);
    // req = req.clone({ headers });
    req = req.clone({
      setHeaders: {
        'Authorization' : 'Bearer ' +  accessToken
        // Authorization: `Bearer ${this.oauthService.getAccessToken()}`
      }
    });
    return next.handle(req);
  }
}
















// import { Injectable, Optional } from '@angular/core';
// import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
// // import { OAuthService, OAuthStorage, OAuthResourceServerErrorHandler } from 'angular-oauth2-oidc';
// import { Observable, of, merge } from 'rxjs';
// import { authConfig, OAuthModuleConfig } from './auth.config';
// import { catchError, filter, timeout, map, take, mergeMap } from 'rxjs/operators';
// import { OAuthStorage } from 'angular-oauth2-oidc';
// @Injectable({
//   providedIn: 'root'
// })

// export class AuthInterceptorService implements HttpInterceptor {

//   constructor() {}

//   intercept(req: HttpRequest<any>, next:HttpHandler): Observable<HttpEvent<any>>{

//     let token =localStorage.getItem('access_token');

//     // console.log("Interceptor request headers: "+JSON.stringify(req.headers));
//     // if(!this.oAuthStorage.getItem('access_token') || !this.oAuthStorage.getItem('access_token') ){
//     //   return next.handle(req);
//     // }

//     console.log("Inside Interceptor accesstoken : " + token);
//     const header = 'Bearer ' + token;
//     const headers = req.headers.set('Authorization', header);
//     req = req.clone({ headers });
//     // req = req.clone({
//     //   setHeaders: {
//     //     'Authorization' : 'Bearer ' +  token
//     //     // Authorization: `Bearer ${this.oauthService.getAccessToken()}`
//     //   }
//     // });
//     return next.handle(req);
//   }
// }





// import { Injectable, Optional } from '@angular/core';
// import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from '@angular/common/http';
// // import { OAuthService, OAuthStorage, OAuthResourceServerErrorHandler } from 'angular-oauth2-oidc';
// import { Observable, of, merge } from 'rxjs';
// import { authConfig, OAuthModuleConfig } from './auth.config';
// import { catchError, filter, timeout, map, take, mergeMap } from 'rxjs/operators';
// import { OAuthStorage } from 'angular-oauth2-oidc';
// @Injectable({
//   providedIn: 'root'
// })

// export class AuthInterceptorService implements HttpInterceptor {

//   constructor(private oauthStorage: OAuthStorage ) {}

//   intercept(req: HttpRequest<any>, next:HttpHandler): Observable<HttpEvent<any>>{

//     let token = this.oauthStorage.getItem('access_token');

//     // console.log("Interceptor request headers: "+JSON.stringify(req.headers));
//     // if(!this.oAuthStorage.getItem('access_token') || !this.oAuthStorage.getItem('access_token') ){
//     //   return next.handle(req);
//     // }

//     console.log("Inside Interceptor accesstoken : " + token);
//     req = req.clone({
//       setHeaders: {
//         'Authorization' : 'Bearer ' +  token
//         // Authorization: `Bearer ${this.oauthService.getAccessToken()}`
//       }
//     });
//     // req = req.clone({
//     //   setHeaders: {
//     //     Authorization: `Bearer ${this.oAuthStorage.getItem('access_token')}`
//     //   }
//     // });
//     return next.handle(req);
//   }
// }
















// import { Injectable, Inject, Optional } from '@angular/core';
// import { OAuthService, OAuthStorage } from 'angular-oauth2-oidc';
// import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
// import {Observable} from 'rxjs';
// import { OAuthResourceServerErrorHandler } from "angular-oauth2-oidc";
// import { OAuthModuleConfig } from "angular-oauth2-oidc";
// // import 'rxjs/add/operator/catch';
// import { catchError } from 'rxjs/operators';

// @Injectable()
// export class DefaultOAuthInterceptor implements HttpInterceptor {
    
//     constructor(
//         private authStorage: OAuthStorage,
//         private errorHandler: OAuthResourceServerErrorHandler,
//         @Optional() private moduleConfig: OAuthModuleConfig
//     ) {
//     }

//     private checkUrl(url: string): boolean {
//         let found = this.moduleConfig.resourceServer.allowedUrls.find(u => url.startsWith(u));
//         return !!found;
//     }

//     public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
//         let url = req.url.toLowerCase();

//         if (!this.moduleConfig) return next.handle(req);
//         if (!this.moduleConfig.resourceServer) return next.handle(req);
//         if (!this.moduleConfig.resourceServer.allowedUrls) return next.handle(req);
//         if (!this.checkUrl(url)) return next.handle(req);

//         let sendAccessToken = this.moduleConfig.resourceServer.sendAccessToken;
        
//         if (sendAccessToken) {

//             let token = this.authStorage.getItem('access_token');
//             let header = 'Bearer ' + token;

//             let headers = req.headers
//                                 .set('Authorization', header);

//             req = req.clone({ headers });
//         }

//         return next.handle(req).pipe(catchError(err => this.errorHandler.handleError(err)));

//     }

// }

// import { Injectable, Optional } from '@angular/core';

// import {
//   HttpEvent,
//   HttpHandler,
//   HttpInterceptor,
//   HttpRequest,
// } from '@angular/common/http';
// import { Observable, of, merge } from 'rxjs';
// import {
//   catchError,
//   filter,
//   map,
//   take,
//   mergeMap,
//   timeout,
// } from 'rxjs/operators';
// import { OAuthService, OAuthResourceServerErrorHandler } from 'angular-oauth2-oidc';
// import { OAuthModuleConfig } from '../services/auth.config';

// @Injectable()
// export class AuthInterceptorService implements HttpInterceptor {
//   constructor(
//     private oAuthService: OAuthService,
//     private errorHandler: OAuthResourceServerErrorHandler,
//     @Optional() private moduleConfig: OAuthModuleConfig
//   ) {}

//   private checkUrl(url: string): boolean {
//     if (this.moduleConfig.resourceServer.customUrlValidation) {
//       return this.moduleConfig.resourceServer.customUrlValidation(url);
//     }

//     if (this.moduleConfig.resourceServer.allowedUrls) {
//       return !!this.moduleConfig.resourceServer.allowedUrls.find((u) =>
//         url.toLowerCase().startsWith(u.toLowerCase())
//       );
//     }

//     return true;
//   }

//   public intercept(
//     req: HttpRequest<any>,
//     next: HttpHandler
//   ): Observable<HttpEvent<any>> {
//     const url = req.url.toLowerCase();

//     if (
//       !this.moduleConfig ||
//       !this.moduleConfig.resourceServer ||
//       !this.checkUrl(url)
//     ) {
//       return next.handle(req);
//     }

//     const sendAccessToken = this.moduleConfig.resourceServer.sendAccessToken;

//     if (!sendAccessToken) {
//       return next
//         .handle(req)
//         .pipe(catchError((err) => this.errorHandler.handleError(err)));
//     }

//     return merge(
//       of(this.oAuthService.getAccessToken()).pipe(filter((token) => !!token)),
//       this.oAuthService.events.pipe(
//         filter((e) => e.type === 'token_received'),
//         timeout(this.oAuthService.waitForTokenInMsec || 0),
//         catchError((_) => of(null)), // timeout is not an error
//         map((_) => this.oAuthService.getAccessToken())
//       )
//     ).pipe(
//       take(1),
//       mergeMap((token) => {
//         if (token) {
//           const header = 'Bearer ' + token;
//           const headers = req.headers.set('Authorization', header);
//           req = req.clone({ headers });
//         }

//         return next
//           .handle(req)
//           .pipe(catchError((err) => this.errorHandler.handleError(err)));
//       })
//     );
//   }
// }

// // @Injectable()
// export class AuthInterceptorService implements HttpInterceptor {
//   constructor(
//     private authStorage: OAuthStorage,
//     private oAuthService: OAuthService,
//     private errorHandler: OAuthResourceServerErrorHandler,
//     @Optional() private moduleConfig: OAuthModuleConfig
//   ) {
//   }

//   private checkUrl(url: string): boolean {
//     if (this.moduleConfig.resourceServer.customUrlValidation) {
//       return this.moduleConfig.resourceServer.customUrlValidation(url);
//     }

//     if (this.moduleConfig.resourceServer.allowedUrls) {
//       return !!this.moduleConfig.resourceServer.allowedUrls.find(u =>
//         url.startsWith(u)
//       );
//     }

//     return true;
//   }

//   public intercept(
//     req: HttpRequest<any>,
//     next: HttpHandler
//   ): Observable<HttpEvent<any>> {
//     const url = req.url.toLowerCase();

//     // if (
//     //   !this.moduleConfig ||
//     //   !this.moduleConfig.resourceServer ||
//     //   !this.checkUrl(url)
//     // ) {
//     //   return next.handle(req);
//     // }

//     const sendAccessToken = this.moduleConfig.resourceServer.sendAccessToken;

//     if (!sendAccessToken) {
//       return next
//         .handle(req)
//         .pipe(catchError(err => this.errorHandler.handleError(err)));
//     }

//     return merge(
//       of(this.oAuthService.getAccessToken()).pipe(
//         filter(token => (token ? true : false))
//       ),
//       this.oAuthService.events.pipe(
//         filter(e => e.type === 'token_received'),
//         timeout(this.oAuthService.waitForTokenInMsec || 0),
//         catchError(_ => of(null)), // timeout is not an error
//         map(_ => this.oAuthService.getAccessToken())
//       )
//     ).pipe(
//       take(1),
//       mergeMap(token => {
//         if (token) {
//           const header = 'Bearer ' + token;
//           const headers = req.headers.set('Authorization', header);
//           req = req.clone({headers});
//         }

//         return next
//           .handle(req)
//           .pipe(catchError(err => this.errorHandler.handleError(err)));
//       })
//     );
//   }
// }
