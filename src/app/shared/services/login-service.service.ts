/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  //apiUrl = 'http://35.238.130.76.nip.io';
  //apiUrl = 'http://localhost:8383'
  private loggedIn = new BehaviorSubject<boolean>(false);
  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }
  private headerDict = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Allow-Origin': '*'
  }
  private requestOptions = {
    headers: new HttpHeaders(this.headerDict),
    method: 'GET,POST',
    mode: 'no-cors'
  };
  constructor(private http: HttpClient) { }
  public authenticate(email: string, password: string): Observable<any> {
    return this.http.post('/databases/mysql/cluster/validateLogin', { email, password }, this.requestOptions)
      .pipe(
        map((response) => {
          return response;
        }));
  }
}
