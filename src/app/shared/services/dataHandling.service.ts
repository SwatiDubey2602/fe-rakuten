/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { OAuthService } from 'angular-oauth2-oidc';

const tokenName = 'token';

@Injectable({
  providedIn: 'root',
})
export class dataHandling {
  user:any;
  claims: any;
  
  private headerDict = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Headers': 'Content-Type',
    'Access-Control-Allow-Origin': '*',
    'created-by': this.user
  }
  private requestOptions = {
    headers: new HttpHeaders(this.headerDict),
    method: 'GET,POST,PUT', // GET, POST, PUT, DELETE
    mode: 'no-cors' 
  };
  private apiUrl = `${environment.apiBaseUrl}`;
  public apiUrlCluster = `${environment.apiUrlCluster}`;
  public apiUrlDataCenter = `${environment.apiUrlDataCenter}`;
  public apiGetFlavor = `${environment.apiGetFlavor}`;
  //public apiGetUserProfile = `${environment.apiGetUserProfile}`;

  constructor(private http: HttpClient, private oauthService:OAuthService) 
  {
    this.claims = this.oauthService.getIdentityClaims();
    this.user = this.claims['preferred_username'];  
  }
  public createDB(data): Observable<any> {
    return this.http.post(this.apiUrl, data,this.requestOptions)
      .pipe( 
        map((response) => {  
          return response;
        }));
  }
  public viewDB(): Observable<any> {
    return this.http.get(this.apiUrl, this.requestOptions)
      .pipe(
        map((response) => {  
          return response;
        }));
  }
  public getFlavor(): Observable<any> {
    return this.http.get(this.apiGetFlavor, this.requestOptions)
      .pipe(
        map((response) => {  
          return response;
        }));
  }
  public getDetailData(id): Observable<any> {
    return this.http.get(this.apiUrl+'/'+id, this.requestOptions)
      .pipe(
        map((response) => {
          console.log(response);
          return response;
        }));
  }
  public bulkImportCluster(data): Observable<any> {
    return this.http.post(this.apiUrlCluster, data,this.requestOptions)
      .pipe( 
        map((response) => {  
          return response;
        }));
  }
  public bulkImportDataCenter(data): Observable<any> {
    return this.http.post(this.apiUrlDataCenter, data,this.requestOptions)
      .pipe( 
        map((response) => {  
          return response;   
        }));
  }
  public getDataCenter(): Observable<any> {
    return this.http.get(this.apiUrlDataCenter, this.requestOptions)
      .pipe( 
        map((response) => {  
          return response;
        }));
  }
  public getCluster(): Observable<any> {
    return this.http.get(this.apiUrlCluster, this.requestOptions)
      .pipe( 
        map((response) => {  
          return response;
        }));
  }
   public viewCluster(): Observable<any> {
    return this.http.get(this.apiUrlCluster, this.requestOptions)
      .pipe( 
        map((response) => {  
          return response;
        }));
  }
  public viewDataCenter(): Observable<any> {
    return this.http.get(this.apiUrlDataCenter, this.requestOptions)
      .pipe( 
        map((response) => {  
          return response;
        }));
  }
 public deleteData(id): Observable<any> {
    return this.http.delete(this.apiUrl+'/'+id, this.requestOptions)
      .pipe(
        map((response) => {
            return response;    
        }));   
} 
  public viewBackup(id): Observable<any> {
    return this.http.get(this.apiUrl+'/'+id+'/backups', this.requestOptions)
    .pipe( 
      map((response) => {  
        return response;
      }));     
  } 
  public updateBackup(id,data): Observable<any> {
    return this.http.put(this.apiUrl+'/'+id, data ,this.requestOptions)
    .pipe( 
      map((response) => {  
        return response;
      }));      
  } 
  public viewBackupPolicy(id): Observable<any> {
    return this.http.get(this.apiUrl+'/'+id, this.requestOptions)
    .pipe( 
      map((response) => {  
        return response;
      }));  
  } 
  public getIframe(value): Observable<any> {
    return this.http.get(value, this.requestOptions)
    .pipe( 
      map((response) => {  
        return response;
      }));  
  } 
  public postGrafana(id,data): Observable<any> {
    return this.http.patch(this.apiUrl+'/'+id, data ,this.requestOptions)
    .pipe( 
      map((response) => {  
        return response;
      }));      
  } 
  public restorePost(id,seqId,data): Observable<any> {
    return this.http.post(this.apiUrl+'/'+id+'/backups/'+seqId+'/restore', data ,this.requestOptions)
    .pipe( 
      map((response) => {  
        return response;
      }));      
  } 
  public restoreGet(id,seqId): Observable<any> {
    return this.http.get(this.apiUrl+'/'+id+'/backups/'+seqId+'/restore/logs', this.requestOptions)
    .pipe( 
      map((response) => {  
        return response;
      }));      
  } 
 
  // public getUserProfileData(): Observable<any> {
  //   return this.http.get(this.apiGetUserProfile, this.requestOptions)
  //     .pipe(
  //       map((response) => {
  //         return response;
  //       }));
  // }
}


