/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

import { Component, OnInit } from '@angular/core';
import { LoginComponent } from '../../../modules/login/login.component';
import { Observable, Subscription, timer } from 'rxjs';
import { LoginService } from 'src/app/shared/services/login-service.service';
import { ActivatedRoute, Router,NavigationEnd } from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isShow = false;
  user:any;
  claims: any;
  isExpanded = true;
  showSubmenu: boolean = true;
  isShowing = false;
  showSubSubMenu: boolean = true;
  UserImage:any;
  subscription: Subscription;
  response:any;
  everyFiveSeconds: Observable<number> = timer(0, 5000);
  routeUrl:any;
  showHideNav(event) {
    this.isShow = !(event instanceof LoginComponent);
  }
  constructor(
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private loginservice: LoginService,
                private oauthService:OAuthService
              ) { }

  ngOnInit() {
    this.claims = this.oauthService.getIdentityClaims();
    this.user = this.claims['preferred_username']; 
    // this.user = JSON.parse(localStorage.getItem('currentUser'));
 /*   this.router.events.pipe(filter((event: NavigationEnd) => event instanceof NavigationEnd)).subscribe(event => 
    {  console.log('this is what your looking for ', event.url); 
     this.routeUrl = event.url;
     if(this.routeUrl === '/clusters' ){
      //this.isExpanded = false;
      this.isShowing = true;
    }
  });
   */

  /*  this.subscription = this.everyFiveSeconds.subscribe(() => {
    if(this.keyCloakService.getKeycloakInstance().isTokenExpired()){
      alert("Session has expired. You will be redirected to login page.")
    }
    });
  
   this.user = this.keyCloakService.getUsername();
   console.log(this.keyCloakService.getKeycloakInstance().idToken);
   console.log(this.keyCloakService.getKeycloakInstance().realmAccess.roles); */
  }
  logout() {
    this.oauthService.logOut();
  }
 
/*   logout() {
    // localStorage.removeItem('currentUser');
    // this.router.navigate(['/login']);
    this.oauthService.logOut(); //Keycloak-OIDC logout implementation

  } */
  /* logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
  } */
  // showUserProfile(){
  //   console.log("User Profile Working");
  // }
  mouseenter() {
    if (!this.isExpanded) {
      this.isShowing = true;
    }
  }

  mouseleave() {
    if (!this.isExpanded) {
      this.isShowing = false;
    }
  }
}
