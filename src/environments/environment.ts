/*

*Copyright (c) 2021 Rakuten, Inc. All Rights Reserved

*

*Unauthorised copying of this file, via any medium is strictly prohibited

*Proprietary and confidential

*/

export const environment = {
  production: false,
  // apiBaseUrl: 'https://dev-apigw.rmnkiba.local/apim/pass/1.0/databases/mysql/cluster',
  // apiUrlCluster:'https://dev-apigw.rmnkiba.local/apim/pass/1.0/system/underlay/cluster',
  // apiUrlDataCenter:'https://dev-apigw.rmnkiba.local/apim/pass/1.0/system/underlay/datacenter',
  // apiGetFlavor: 'https://dev-apigw.rmnkiba.local/apim/pass/1.0/system/catalog/products',
  // loginApi:'https://dev-apigw.rmnkiba.local/apim/pass/1.0/databases/mysql/cluster/validateLogin',

  apiBaseUrl: 'http://localhost:8383/databases/mysql/cluster',
  apiUrlCluster:'http://localhost:8383/system/underlay/cluster',
  apiUrlDataCenter:'http://localhost:8383/system/underlay/datacenter',
  apiGetFlavor: 'http://localhost:8383/system/catalog/products',
  //apiGetUserProfile: 'http://localhost:8383',

  //Keycloak environment configuration
  keycloak: {
    //Local Version
    issuer: 'http://localhost:8080/auth/realms/RCP-SB',
    redirectUri: 'http://localhost:4200/viewDB',
    clientId: 'PaaS',
    responseType: 'code', //Authorization code flow OIDC PKCE
    scope: 'openid profile email',
    postLogoutRedirectUri: 'http://localhost:4200/',
    redirectUriAsPostLogoutRedirectUriFallback: false,
    requireHttps: false,
    showDebugInformation: true,
    disableAtHashCheck: true

    // //Sandbox version
    // issuer: 'https://stg-s-oss.rmb-lab.jp/auth/realms/RCP-SB',
    // redirectUri: 'http://[240b:c0e0:102:54C0:1c05:c2c1:12:500b]/viewDB',
    // clientId: 'PaaS',
    // responseType: 'code', //Authorization code flow OIDC PKCE
    // scope: 'openid profile email',
    // postLogoutRedirectUri: 'http://[240b:c0e0:102:54C0:1c05:c2c1:12:500b]/',
    // redirectUriAsPostLogoutRedirectUriFallback: false,
    // requireHttps: false,
    // showDebugInformation: true,
    // disableAtHashCheck: true 
  }
};

